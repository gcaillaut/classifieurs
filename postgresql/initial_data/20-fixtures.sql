create extension if not exists pgcrypto;

insert into Users (username, password, allowed_trials_count) values ('enseignant', crypt('ensIUT45', gen_salt('bf')), 1000);
