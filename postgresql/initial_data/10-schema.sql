-- drop table if exists Statistics;
-- drop table if exists Results;
-- drop table if exists Trials;
-- drop table if exists Users;


create table if not exists Users (
    id serial primary key not null,
    username varchar(30) unique not null,
    password text not null,
    allowed_trials_count integer not null
);


create table if not exists Trials (
    id serial primary key not null,
    user_id integer references Users(id) on delete cascade,
    training_error double precision not null,
    date timestamp not null
);


create table if not exists Results (
    id serial primary key not null,
    class_label text not null,
    trial_id integer references Trials(id) on delete cascade,
    unique(class_label, trial_id)
);


create table if not exists Statistics (
    id serial primary key not null,
    true_positive_count integer not null,
    true_positive_rate double precision not null,
    precision double precision not null,
    recall double precision not null,
    fmeasure double precision not null,
    result_id integer references Results(id) on delete cascade
);