library(shiny)
library(shinydashboard)

library(tidyverse)
library(magrittr)
library(lubridate)

# library(rdrop2)
library(glue)

# Allowed packages
# library(tm)
# library(e1071)
# library(rpart)
# library(nnet)


source("ui/ui.R")
source("server/server.R")

shinyApp(ui, server)
