condition <- function(subclass, message, call = sys.call(-1), ...) {
  structure(
    class = c(subclass, "condition"),
    list(message = message, call = call, ...)
  )
}


file_not_found_error <- function(file) {
  condition(c("file_not_found_error", "error"),
    message = glue("File not found: {file}"),
    file = file
  )
}


function_does_not_exist_error <- function(fun_name) {
  condition(c("function_does_not_exist_error", "error"),
    message = glue("Function does not exist: {fun_name}"),
    fun_name = fun_name
  )
}


evaluation_context <- function(path) {
  sourced_file <- "main.R"
  if (!sourced_file %in% list.files(path)) {
    stop(file_not_found_error(sourced_file))
  }

  previous_wd <- getwd()
  exec_env <- new.env()

  setwd(path)
  source(sourced_file, local = exec_env)

  evaluated_function <- "classer"
  if (!evaluated_function %in% ls(exec_env)) {
    setwd(previous_wd)
    stop(function_does_not_exist_error(evaluated_function))
  }

  list(previous_wd = previous_wd, exec_env = exec_env)
}


restore_context <- function(context) {
  setwd(context$previous_wd)
}


evaluate_file <- function(filepath, env) {
  assign("filepath", filepath, envir = env)
  tryCatch(
    eval(quote(classer(filepath)), envir = env),
    error = function(err) err
  )
}
